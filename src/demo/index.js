/* eslint-disable no-console */

import './styles.scss';
import { show } from '../save-to-favorites-dialog';

document.querySelectorAll('[data-track-btn]').forEach((el) => {
  const id = el.getAttribute('data-track-btn');
  const title = el.getAttribute('data-track-title');
  const onClick = () => {
    showDialog(id, title);
  };
  el.addEventListener('click', onClick, false);
});

function showDialog(trackId, trackTitle) {
  const trackData = {
    id: trackId,
    title: trackTitle
  };

  const options = {
    track: trackData,
    onCancel: () => {
      console.log('cancelled');
    },
    onSuccess: (result) => {
      console.log('success', result);
      const btn = document.querySelector(`[data-track-btn="${trackId}"]`);
      if (result.isFavorite) {
        btn.setAttribute('data-favorite', '');
      } else {
        btn.removeAttribute('data-favorite');
      }
    }
  };

  // window.lu.ui.saveToFavoritesDialog.show(options);
  show(options);
}
