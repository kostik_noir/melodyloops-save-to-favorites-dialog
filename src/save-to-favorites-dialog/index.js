import { registerInGlobalNamespace } from './utils';
import * as dialog from './dialog';

registerInGlobalNamespace('lu.ui.saveToFavoritesDialog', { show: dialog.show });

export const { show } = dialog;
