import { injectLocalClassNames } from '../../../utils';
import styles from './styles.scss';
import tpl from './tpl.html';

export function getHtml() {
  return injectLocalClassNames(tpl, styles);
}
