import {
  htmlToNode,
  injectLocalClassNames,
  getScrollbarWidth,
  createVerticalScrollbarWatcher,
  injectCssStyles
} from '../../../utils';
import styles from './styles.scss';
import tpl from './tpl.html';
import listItemTpl from './list-item.tpl.html';

const stateWithEmptyItemsId = 1;
const stateCreateNewItemId = 2;
const stateWithItemsId = 3;
const stateNoSearchMatchesId = 4;

const activeStateAttrName = 'data-is-active-state';
const withVisibleVerticalScrollbarAttrName = 'data-with-visible-vertical-scrollbar';

let areExtraStylesInjected = false;

export function create(items = [], trackId) {
  let isActive = true;
  let onAddListeners = [];
  let onRemoveListeners = [];

  injectExtraStyles();

  const node = htmlToNode(tpl, styles);

  const ctx = {
    items,
    node,
    trackId,
    notifyOnAddListeners,
    notifyOnRemoveListeners
  };
  findState(items).factory(ctx);

  return {
    destroy,
    getNode,
    filter,
    onAdd: registerOnAddListener,
    onRemove: registerOnRemoveListener
  };

  function destroy() {
    if (!isActive) {
      return;
    }
    isActive = false;
    ctx.state.exit();
  }

  function getNode() {
    return node;
  }

  function filter(v) {
    ctx.state.filter(v);
  }

  function registerOnAddListener(fn) {
    const unsubscribe = () => {
      onAddListeners = onAddListeners.filter(v => v !== fn);
    };
    onAddListeners.push(fn);
    return unsubscribe;
  }

  function notifyOnAddListeners(id) {
    onAddListeners.forEach(fn => fn(id));
  }

  function registerOnRemoveListener(fn) {
    const unsubscribe = () => {
      onRemoveListeners = onRemoveListeners.filter(v => v !== fn);
    };
    onRemoveListeners.push(fn);
    return unsubscribe;
  }

  function notifyOnRemoveListeners(id) {
    onRemoveListeners.forEach(fn => fn(id));
  }
}

function getStateId(items, searchValue) {
  if (items.length === 0) {
    if (searchValue === '') {
      return stateWithEmptyItemsId;
    }
    return stateCreateNewItemId;
  }

  if (searchValue === '') {
    return stateWithItemsId;
  }

  const hasMatches = items.some(({ title }) => new RegExp(searchValue, 'gi').test(title));
  if (hasMatches) {
    return stateWithItemsId;
  }
  return stateNoSearchMatchesId;
}

function findState(items, searchValue = '') {
  const id = getStateId(items, searchValue);

  switch (id) {
    case stateWithEmptyItemsId:
      return {
        id,
        factory: stateWithEmptyItems
      };
    case stateCreateNewItemId:
      return {
        id,
        factory: stateCreateNewItem
      };
    case stateWithItemsId:
      return {
        id,
        factory: stateWithItems
      };
    case stateNoSearchMatchesId:
      return {
        id,
        factory: stateNoSearchMatches
      };
  }

  return null;
}

function stateWithEmptyItems(ctx) {
  const stateId = stateWithEmptyItemsId;
  let isActive = true;

  ctx.state = {
    exit,
    filter
  };

  const node = ctx.node.querySelector('[data-state-with-empty-items]');
  node.setAttribute(activeStateAttrName, '');

  function exit() {
    if (!isActive) {
      return;
    }
    isActive = false;
    node.removeAttribute(activeStateAttrName);
  }

  function filter(v) {
    const { id: nextStateId, factory } = findState(ctx.items, v);
    if (nextStateId !== stateId) {
      exit();
      factory(ctx);
    }
  }
}

function stateCreateNewItem(ctx) {
  const stateId = stateCreateNewItemId;
  let isActive = true;

  ctx.state = {
    exit,
    filter
  };

  const node = ctx.node.querySelector('[data-state-create-new-item]');
  node.setAttribute(activeStateAttrName, '');

  function exit() {
    if (!isActive) {
      return;
    }
    isActive = false;
    node.removeAttribute(activeStateAttrName);
  }

  function filter(v) {
    const { id: nextStateId, factory } = findState(ctx.items, v);
    if (nextStateId !== stateId) {
      exit();
      factory(ctx);
    }
  }
}

function stateNoSearchMatches(ctx) {
  const stateId = stateNoSearchMatchesId;
  let isActive = true;

  ctx.state = {
    exit,
    filter
  };

  const node = ctx.node.querySelector('[data-state-no-search-matches]');
  node.setAttribute(activeStateAttrName, '');

  function exit() {
    if (!isActive) {
      return;
    }
    isActive = false;
    node.removeAttribute(activeStateAttrName);
  }

  function filter(v) {
    const { id: nextStateId, factory } = findState(ctx.items, v);
    if (nextStateId !== stateId) {
      exit();
      factory(ctx);
    }
  }
}

function stateWithItems(ctx) {
  const stateId = stateWithItemsId;
  let isActive = true;

  ctx.state = {
    exit,
    filter
  };

  const node = ctx.node.querySelector('[data-state-with-items]');
  node.setAttribute(activeStateAttrName, '');

  const listNode = node.querySelector('[data-list]');
  listNode.innerHTML = ctx.items.reduce((acc, obj) => [...acc, ...generateListItemHtml(obj, ctx.trackId)], []).join('');

  const items = listNode.querySelectorAll('[data-item]');
  items.forEach((el) => {
    el.addEventListener('click', onListItemPressed, false);
  });

  const verticalScrollbarWatcher = createVerticalScrollbarWatcher(listNode);
  verticalScrollbarWatcher.onChange(updateListItems);

  // when it will be attached into document
  setTimeout(() => {
    verticalScrollbarWatcher.run();
    updateListItems();
  }, 0);

  function exit() {
    if (!isActive) {
      return;
    }
    isActive = false;

    items.forEach((el) => {
      el.removeEventListener('click', onListItemPressed, false);
    });

    node.removeAttribute(activeStateAttrName);
    listNode.innerHTML = '';

    verticalScrollbarWatcher.stop();
  }

  function filter(v) {
    const { id: nextStateId, factory } = findState(ctx.items, v);
    if (nextStateId !== stateId) {
      exit();
      factory(ctx);
      return;
    }

    const isHiddenAttrName = 'data-is-hidden';
    items.forEach((el) => {
      const regExp = new RegExp(v, 'gi');
      const title = el.getAttribute('data-title');
      const isHidden = !regExp.test(title);
      if (isHidden) {
        el.setAttribute(isHiddenAttrName, '');
      } else {
        el.removeAttribute(isHiddenAttrName);
      }
    });

    verticalScrollbarWatcher.forceUpdate();
  }

  function onListItemPressed(event) {
    const { currentTarget } = event;
    const id = currentTarget.getAttribute('data-id');
    if (currentTarget.getAttribute('data-contains-track') === 'true') {
      ctx.notifyOnRemoveListeners(id);
    } else {
      ctx.notifyOnAddListeners(id);
    }
  }

  function updateListItems() {
    const hasVerticalScrollbar = verticalScrollbarWatcher.isVisible();
    if (hasVerticalScrollbar) {
      listNode.setAttribute(withVisibleVerticalScrollbarAttrName, '');
    } else {
      listNode.removeAttribute(withVisibleVerticalScrollbarAttrName);
    }
  }
}

function generateListItemHtml({ id, title, tracks }, trackId) {
  const isTrackAddedToPlaylist = tracks.indexOf(trackId) !== -1 || tracks.indexOf(`${trackId}`) !== -1;

  return injectLocalClassNames(listItemTpl, styles)
    .replace(/{\s*TITLE\s*}/gi, title)
    .replace(/{\s*ID\s*}/gi, id)
    .replace(/{\s*CONTAINS_TRACK\s*}/gi, `${isTrackAddedToPlaylist}`);
}

function injectExtraStyles() {
  if (areExtraStylesInjected) {
    return; // eslint-disable-line consistent-return
  }
  areExtraStylesInjected = true;

  const paddingRight = 40 - getScrollbarWidth();
  const cssCode = `
    .${styles.list}[${withVisibleVerticalScrollbarAttrName}] .${styles.listItem} {
      padding-right: ${paddingRight}px;
    }
  `;
  injectCssStyles(cssCode);
}
