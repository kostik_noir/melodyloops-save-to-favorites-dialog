import { htmlToNode } from '../../../utils';
import tpl from './tpl.html';
import styles from './styles.scss';

export function create() {
  let isActive = true;
  let hasActiveInput = false;
  let extraClassNames = [];
  let onChangeListeners = [];
  let onSubmitListeners = [];

  const node = htmlToNode(tpl, styles);

  const input = node.querySelector('[data-input]');
  input.addEventListener('input', onInputValueChanged, false);
  input.addEventListener('focus', onActiveInput, false);
  input.addEventListener('blur', onInactiveInput, false);
  input.addEventListener('keydown', onInputKeyDown, false);
  input.value = '';

  const clearBtn = node.querySelector('[data-clear-btn]');
  clearBtn.addEventListener('click', onClearBtnPressed, false);

  const submitBtn = node.querySelector('[data-submit-btn]');
  submitBtn.addEventListener('click', onSubmitBtnPressed, false);

  updateUiState();

  setTimeout(() => {
    input.focus();
  }, 0);

  return {
    destroy,
    getNode: () => node,
    getValue: () => input.value,
    onChange: registerOnChangeListener,
    onSubmit: registerOnSubmitListener
  };

  function destroy() {
    if (!isActive) {
      return;
    }
    isActive = false;

    clearBtn.removeEventListener('click', onClearBtnPressed, false);
    input.removeEventListener('change', onInputValueChanged, false);
    input.removeEventListener('focus', onActiveInput, false);
    input.removeEventListener('blur', onInactiveInput, false);
    input.removeEventListener('keydown', onInputKeyDown, false);
    submitBtn.removeEventListener('click', onSubmitBtnPressed, false);
  }

  function registerOnChangeListener(fn) {
    const unsubscribe = () => {
      onChangeListeners = onChangeListeners.filter(v => v !== fn);
    };
    onChangeListeners.push(fn);
    return unsubscribe;
  }

  function registerOnSubmitListener(fn) {
    const unsubscribe = () => {
      onSubmitListeners = onSubmitListeners.filter(v => v !== fn);
    };
    onSubmitListeners.push(fn);
    return unsubscribe;
  }

  function onSubmitBtnPressed() {
    if (input.value === '') {
      return;
    }
    notifyOnSubmitListeners();
  }

  function onInputKeyDown(event) {
    if (event.keyCode !== 13 || input.value === '') {
      return;
    }
    input.blur();
    notifyOnSubmitListeners();
  }

  function notifyOnSubmitListeners() {
    onSubmitListeners.forEach(fn => fn());
  }

  function onClearBtnPressed() {
    input.value = '';
    onInputValueChanged();
  }

  function notifyOnChangeListeners() {
    const { value } = input;
    onChangeListeners.forEach(fn => fn(value));
  }

  function onInputValueChanged() {
    notifyOnChangeListeners();
    updateUiState();
  }

  function onActiveInput() {
    hasActiveInput = true;
    updateUiState();
  }

  function onInactiveInput() {
    hasActiveInput = false;
    updateUiState();
  }

  function updateUiState() {
    extraClassNames.forEach((className) => {
      node.classList.remove(className);
    });

    extraClassNames = [];
    extraClassNames.push(input.value === '' ? styles.withEmptyValue : styles.withNotEmptyValue);
    if (hasActiveInput) {
      extraClassNames.push(styles.isActive);
    }

    extraClassNames.forEach((className) => {
      node.classList.add(className);
    });
  }
}
