import { htmlToNode } from '../../utils';
import { create as createSearchInput } from '../widgets/search-input';
import { create as createPlaylists } from '../widgets/playlists';
import * as spinner from '../widgets/spinner';
import * as saveDataService from '../../save-data-service';
import styles from './styles.scss';
import tpl from './tpl.html';

export function enter(ctx) {
  let isActive = true;

  ctx.state = { exit };

  const { data: { track } } = ctx;

  const tplHtml = tpl.replace(/{{\s*SPINNER\s*}}/gi, spinner.getHtml());
  const node = htmlToNode(tplHtml, styles);
  ctx.parentNode.appendChild(node);

  const searchInput = createSearchInput();
  node.querySelector('[data-search-input-container]').appendChild(searchInput.getNode());

  const playlists = createPlaylists(ctx.data.playlists, track.id);
  node.querySelector('[data-playlists-container]').appendChild(playlists.getNode());

  searchInput.onChange(onSearchInputChanged);
  searchInput.onSubmit(onSearchInputSubmitted);
  playlists.onAdd(onPlaylistSelectedAsFavorite);
  playlists.onRemove(onPlaylistDeselectedAsFavorite);

  function exit() {
    if (!isActive) {
      return;
    }
    isActive = false;

    searchInput.destroy();
    playlists.destroy();
    ctx.parentNode.removeChild(node);
  }

  function onSearchInputChanged() {
    playlists.filter(searchInput.getValue());
  }

  function onSearchInputSubmitted() {
    const searchValue = searchInput.getValue();
    const playlist = ctx.data.playlists.filter(({ title }) => title === searchValue)[0];
    const isNew = typeof playlist === 'undefined';

    if (!isNew && playlist.tracks.indexOf(`${track.id}`) !== -1) {
      return;
    }

    const data = {
      track
    };

    if (isNew) {
      data.playlist = {
        title: searchValue
      };
    } else {
      data.playlist = playlist;
    }

    addTrackToPlaylistAndCloseDialog(data);
  }

  function onPlaylistSelectedAsFavorite(playlistId) {
    const playlist = ctx.data.playlists.filter(({ id }) => `${id}` === playlistId)[0];
    if (typeof playlist === 'undefined') {
      return;
    }

    const data = {
      playlist,
      track
    };
    addTrackToPlaylistAndCloseDialog(data);
  }

  function onPlaylistDeselectedAsFavorite(playlistId) {
    const playlist = ctx.data.playlists.filter(({ id }) => `${id}` === playlistId)[0];
    if (typeof playlist === 'undefined') {
      return;
    }

    const data = {
      playlist,
      track
    };
    removeTrackFromPlaylistAndCloseDialog(data);
  }

  function addTrackToPlaylistAndCloseDialog(data) {
    ctx.closeDialog();

    const { onCancel, onSuccess } = ctx;
    saveDataService.addTrackToPlaylist({ data, onCancel, onSuccess });
  }

  function removeTrackFromPlaylistAndCloseDialog(data) {
    ctx.closeDialog();

    const { onCancel, onSuccess } = ctx;
    saveDataService.removeTrackFromPlaylist({ data, onCancel, onSuccess });
  }
}
