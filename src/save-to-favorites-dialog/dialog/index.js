import * as modal from '../modal';
import { getNode as createRootNode } from './root-node';
import * as stateLoadData from './state-load-data';

export function show(options) {
  if (!modal.canShow()) {
    throw new Error('Can\'t show dialog "Save to Favorites"');
  }

  const { track } = options;
  const onCancel = typeof options.onCancel === 'function' ? options.onCancel : () => {};
  const onSuccess = typeof options.onSuccess === 'function' ? options.onSuccess : () => {};
  ensureTrackDataIsValid(track);

  const rootNode = createRootNode();

  const modalApi = modal.show({
    title: 'Save to Favorites',
    subtitle: track.title,
    content: rootNode,
    onClose: onModalClosed
  });

  const ctx = {
    parentNode: rootNode,
    data: {
      track
    },
    closeDialog: modalApi.close,
    onCancel,
    onSuccess
  };

  stateLoadData.enter(ctx);

  function onModalClosed(reasonForClosing = {}) {
    ctx.state.exit();

    if (reasonForClosing.canceledByUser) {
      onCancel();
    }
  }
}

function ensureTrackDataIsValid(data = {}) {
  const { id, title } = data;

  const isValidTrackId = (typeof id === 'string' && id.length > 0) || !Number.isNaN(id);
  if (!isValidTrackId) {
    throw new Error('Invalid track id value');
  }

  const isValidTrackTitle = typeof title === 'string' && title.length > 0;
  if (!isValidTrackTitle) {
    throw new Error('Invalid track title');
  }
}
