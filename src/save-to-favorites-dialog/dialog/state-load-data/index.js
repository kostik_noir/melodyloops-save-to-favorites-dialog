import { htmlToNode } from '../../utils';
import { loadPlaylists } from '../../backend-api';
import * as spinner from '../widgets/spinner';
import * as stateMain from '../state-main';
import styles from './styles.scss';
import tpl from './tpl.html';

export function enter(ctx) {
  let isActive = true;

  ctx.state = { exit };

  const tplHtml = tpl.replace(/{{\s*SPINNER\s*}}/gi, spinner.getHtml());
  const node = htmlToNode(tplHtml, styles);
  ctx.parentNode.appendChild(node);

  const subStateCtx = {
    node,
    onDataLoaded
  };

  stateLoading(subStateCtx);

  function exit() {
    if (!isActive) {
      return;
    }
    isActive = false;

    subStateCtx.state.exit();
    ctx.parentNode.removeChild(node);
  }

  function onDataLoaded(data) {
    exit();
    ctx.data.playlists = data;
    stateMain.enter(ctx);
  }
}

function stateLoading(ctx) {
  let isActive = true;

  ctx.state = { exit };

  const { node } = ctx;
  node.classList.add(styles.isStateLoading);

  loadPlaylists()
    .then((data) => {
      if (!isActive) {
        return;
      }
      exit();
      ctx.onDataLoaded(data);
    })
    .catch(() => {
      exit();
      stateError(ctx);
    });

  function exit() {
    if (!isActive) {
      return;
    }
    isActive = false;
    node.classList.remove(styles.isStateLoading);
  }
}

function stateError(ctx) {
  let isActive = true;

  ctx.state = { exit };

  const { node } = ctx;
  node.classList.add(styles.isStateError);

  const tryAgainBtn = node.querySelector('[data-try-again-btn');
  tryAgainBtn.addEventListener('click', onTryAgainBtnPressed, false);

  function exit() {
    if (!isActive) {
      return;
    }
    isActive = false;

    node.classList.remove(styles.isStateError);
    tryAgainBtn.removeEventListener('click', onTryAgainBtnPressed, false);
  }

  function onTryAgainBtnPressed() {
    exit();
    stateLoading(ctx);
  }
}
