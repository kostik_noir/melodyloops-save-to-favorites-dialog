import { htmlToNode } from '../../utils';
import styles from './styles.scss';
import tpl from './tpl.html';

export function getNode() {
  return htmlToNode(tpl, styles);
}
