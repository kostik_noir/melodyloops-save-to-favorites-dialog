import { htmlToNode } from '../utils';
import styles from './styles.scss';
import tpl from './tpl.html';

let isActive = false;

const isCanceledByUser = { canceledByUser: true };

export function canShow() {
  return !isActive;
}

export function show(data = {}) {
  if (isActive) {
    throw new Error('Modal is already active');
  }
  isActive = true;

  const { title = '', subtitle = '', content } = data;

  const node = htmlToNode(tpl, styles);
  document.body.appendChild(node);

  node.querySelector('[data-title]').innerText = title;

  if (subtitle === '') {
    node.classList.add(styles.withoutSubtitle);
  } else {
    node.querySelector('[data-subtitle]').innerText = subtitle;
  }

  const contentContainer = node.querySelector('[data-body]');
  if (typeof content === 'string') {
    contentContainer.innerHTML = content;
  } else {
    try {
      contentContainer.appendChild(content);
    } catch (e) {
      throw new Error(`Can't insert content ${content}`);
    }
  }

  const closeBtn = node.querySelector('[data-close-btn]');
  closeBtn.addEventListener('click', onCloseBtnPressed, false);

  const overlay = node.querySelector('[data-overlay]');
  overlay.addEventListener('click', onOverlayPressed, false);

  document.addEventListener('keydown', onKeyDown, false);

  return {
    close
  };

  function close(reasonForClosing = {}) {
    if (!isActive) {
      return;
    }
    isActive = false;

    closeBtn.removeEventListener('click', onCloseBtnPressed, false);
    overlay.removeEventListener('click', onOverlayPressed, false);
    document.removeEventListener('keydown', onKeyDown, false);

    document.body.removeChild(node);

    const { onClose } = data;
    if (typeof onClose === 'function') {
      onClose(reasonForClosing);
    }
  }

  function onCloseBtnPressed() {
    close(isCanceledByUser);
  }

  function onOverlayPressed() {
    close(isCanceledByUser);
  }

  function onKeyDown(event) {
    const escKeyCode = 27;
    if (event.isComposing || event.keyCode === 229 || event.keyCode !== escKeyCode) {
      return;
    }

    close(isCanceledByUser);
  }
}
