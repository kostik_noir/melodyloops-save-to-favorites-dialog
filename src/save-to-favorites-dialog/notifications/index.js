import * as iziToast from 'izitoast/src/js/iziToast';
import './styles.scss';

const timeout = 5000;
const position = 'bottomCenter';
const shouldShowProgressBar = false;
const shouldAnimateInnerContent = false;
const transitionIn = 'fadeInUp';
const transitionOut = 'fadeOut';

export function showSuccess(msg) {
  iziToast.success({
    message: msg,
    class: 'iziToast--success',
    timeout,
    position,
    progressBar: shouldShowProgressBar,
    animateInside: shouldAnimateInnerContent,
    transitionIn,
    transitionOut,
    transitionInMobile: transitionIn,
    transitionOutMobile: transitionOut,
    onOpening: (_, node) => {
      resetHardcodedInlineStyles(node);
      changeOrderForTextsAndCloseBtn(node);
    }
  });
}

export function showError({ msg, cancelBtnTitle, submitBtnTitle, onCancel, onSubmit }) {
  iziToast.error({
    message: msg,
    class: 'iziToast--error',
    timeout: false,
    position,
    drag: false,
    progressBar: shouldShowProgressBar,
    animateInside: shouldAnimateInnerContent,
    close: false,
    transitionIn,
    transitionOut,
    transitionInMobile: transitionIn,
    transitionOutMobile: transitionOut,
    buttons: [
      [
        `<div class="iziToast__btn">${submitBtnTitle}</div>`,
        (instance, toast) => {
          instance.hide(null, toast);
          onSubmit();
        }
      ],
      [
        `<div class="iziToast__btn">${cancelBtnTitle}</div>`,
        (instance, toast) => {
          instance.hide(null, toast);
          onCancel();
        }
      ]
    ],
    onOpening: (_, node) => {
      resetHardcodedInlineStyles(node);
      changeOrderForTextsAndCloseBtn(node);
    }
  });
}

function resetHardcodedInlineStyles(node) {
  [
    node,
    node.querySelector('.iziToast-body'),
    node.querySelector('.iziToast-texts'),
    node.querySelector('.iziToast-message')
  ].forEach((el) => {
    el.style.paddingLeft = '';
    el.style.paddingRight = '';
    el.style.marginLeft = '';
    el.style.marginRight = '';
    el.style.marginTop = '';
    el.style.marginBottom = '';
  });
}

function changeOrderForTextsAndCloseBtn(node) {
  const closeBtn = node.querySelector('.iziToast-close');
  const bodyNode = node.querySelector('.iziToast-body');
  if (closeBtn === null || bodyNode === null || closeBtn.parentNode !== bodyNode.parentNode) {
    return;
  }

  bodyNode.parentNode.insertBefore(closeBtn, bodyNode);
}
