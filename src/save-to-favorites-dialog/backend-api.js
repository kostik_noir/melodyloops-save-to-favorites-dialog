const urlPrefix = 'favorites';

export function loadPlaylists() {
  const url = `${getApiServerRoot()}/${urlPrefix}`;

  return new Promise((resolve, reject) => {
    fetch(url)
      .then(resp => resp.json())
      .then((data) => {
        data.forEach((obj) => {
          if (typeof obj.id === 'undefined') {
            obj.id = obj.title;
          }
        });
        resolve(data);
      })
      .catch(reject);
  });
}

export function addTrackToPlaylist(data) {
  const url = `${getApiServerRoot()}/${urlPrefix}`;

  const {
    track: {
      id: trackId
    },
    playlist: {
      title: playlistTitle
    }
  } = data;

  const postData = {
    trackId,
    playlistTitle
  };

  const cfg = {
    method: 'post',
    body: JSON.stringify(postData),
    headers: {
      'Content-Type': 'application/json;charset=utf-8'
    }
  };

  return new Promise((resolve, reject) => {
    fetch(url, cfg)
      .then(() => {
        resolve({
          isFavorite: true
        });
      })
      .catch(reject);
  });
}

export function removeTrackFromPlaylist(data) {
  const url = `${getApiServerRoot()}/${urlPrefix}`;

  const {
    track: {
      id: trackId
    },
    playlist: {
      title: playlistTitle
    }
  } = data;

  const postData = {
    trackId,
    playlistTitle
  };

  const cfg = {
    method: 'delete',
    body: JSON.stringify(postData),
    headers: {
      'Content-Type': 'application/json;charset=utf-8'
    }
  };

  return new Promise((resolve, reject) => {
    fetch(url, cfg)
      .then(res => res.json())
      .then((data) => {
        resolve({
          isFavorite: data.isFavorite === true
        });
      })
      .catch(reject);
  });
}

function getApiServerRoot() {
  let url = '/get';
  try {
    url = window.mlConfig.API_SERVER;
  } catch (e) {
  }
  return url.replace(/\/*$/gi, '');
}
