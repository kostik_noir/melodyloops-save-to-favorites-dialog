import * as backendApi from './backend-api';
import * as notifications from './notifications';

const typeAddTrackToPlaylist = 1;
const typeRemoveTrackFromPlaylist = 2;

export function addTrackToPlaylist({ data, onCancel, onSuccess }) {
  const ctx = {
    type: typeAddTrackToPlaylist,
    data,
    onCancel,
    onSuccess
  };
  stateSaving(ctx);
}

export function removeTrackFromPlaylist({ data, onCancel, onSuccess }) {
  const ctx = {
    type: typeRemoveTrackFromPlaylist,
    data,
    onCancel,
    onSuccess
  };
  stateSaving(ctx);
}

function stateSaving(ctx) {
  let fn;
  switch (ctx.type) {
    case typeAddTrackToPlaylist:
      fn = backendApi.addTrackToPlaylist;
      break;
    case typeRemoveTrackFromPlaylist:
      fn = backendApi.removeTrackFromPlaylist;
      break;
  }

  fn(ctx.data)
    .then((result) => {
      stateSuccess(ctx, result);
    })
    .catch(() => {
      stateFail(ctx);
    });
}

function stateSuccess(ctx, result) {
  let msg;
  switch (ctx.type) {
    case typeAddTrackToPlaylist:
      msg = `Track "${ctx.data.track.title}" has been added to playlist "${ctx.data.playlist.title}"`;
      break;
    case typeRemoveTrackFromPlaylist:
      msg = `Track "${ctx.data.track.title}" has been removed from playlist "${ctx.data.playlist.title}"`;
      break;
  }

  notifications.showSuccess(msg);

  // XXX we use setTimeout here in order to avoid an erroneous transition to an error state
  setTimeout(() => {
    ctx.onSuccess(result);
  });
}

function stateFail(ctx) {
  const onRetryBtn = () => {
    stateSaving(ctx);
  };

  const onCancelBtn = () => {
    ctx.onCancel();
  };

  let msg;
  switch (ctx.type) {
    case typeAddTrackToPlaylist:
      msg = `Can't add track "${ctx.data.track.title}" to playlist "${ctx.data.playlist.title}"`;
      break;
    case typeRemoveTrackFromPlaylist:
      msg = `Can't remove track "${ctx.data.track.title}" from playlist "${ctx.data.playlist.title}"`;
      break;
  }

  const cfg = {
    msg,
    onSubmit: onRetryBtn,
    onCancel: onCancelBtn,
    submitBtnTitle: 'Try again',
    cancelBtnTitle: 'Cancel'
  };

  notifications.showError(cfg);
}
