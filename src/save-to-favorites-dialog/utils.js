export function registerInGlobalNamespace(path, obj) {
  const parts = path.split('.');

  let parent = window;
  parts.forEach((key) => {
    if (typeof parent[key] === 'undefined') {
      parent[key] = {};
    }
    parent = parent[key];
  });

  Object.keys(obj).forEach((k) => {
    parent[k] = obj[k];
  });
}

export function htmlToNode(html, localClassNamesMap = {}) {
  html = injectLocalClassNames(html, localClassNamesMap);
  const node = document.createElement('div');
  node.innerHTML = html.trim();
  return node.firstChild;
}

export function injectLocalClassNames(htmlStr, localClassNamesMap) {
  const prefix = 'styles.';

  Object.keys(localClassNamesMap).forEach((localClassName) => {
    const globalClassName = localClassNamesMap[localClassName];
    const regExp = new RegExp(`{\\s*${prefix}${localClassName}\\s*}`, 'gi');
    htmlStr = htmlStr.replace(regExp, globalClassName);
  });

  return htmlStr;
}

let scrollBarWidth = Number.NaN;

export function getScrollbarWidth() {
  if (!Number.isNaN(scrollBarWidth)) {
    return scrollBarWidth;
  }

  const parentHeight = 100;

  const parentNode = document.createElement('div');
  parentNode.style.display = 'block';
  parentNode.style.width = '100px';
  parentNode.style.height = `${parentHeight}px`;
  parentNode.style.position = 'absolute';
  parentNode.style.left = '-10000px';
  parentNode.style.top = '-10000px';
  parentNode.style.overflowY = 'scroll';

  const childNode = document.createElement('div');
  childNode.style.width = '100%';
  childNode.style.height = `${parentHeight * 2}px`;

  parentNode.appendChild(childNode);
  document.body.appendChild(parentNode);
  scrollBarWidth = parentNode.offsetWidth - childNode.offsetWidth;
  document.body.removeChild(parentNode);

  return scrollBarWidth;
}

export function createVerticalScrollbarWatcher(el) {
  let isActive = false;

  let timeoutId = Number.NaN;
  const onChangeListeners = [];
  let result = null;

  return {
    stop,
    run,
    forceUpdate,
    isVisible,
    onChange: registerOnChangeListener
  };

  function run() {
    if (isActive) {
      return;
    }
    isActive = true;

    window.addEventListener('resize', onResize, false);
    window.addEventListener('orientationchange', onResize, false);

    result = getResult();
  }

  function stop() {
    if (!isActive) {
      return;
    }
    isActive = false;
    window.removeEventListener('resize', onResize, false);
    window.removeEventListener('orientationchange', onResize, false);
    stopTimeout();
  }

  function forceUpdate() {
    if (!isActive) {
      return;
    }
    stopTimeout();
    updateValue();
  }

  function isVisible() {
    if (result === null) {
      result = getResult();
    }
    return result;
  }

  function getResult() {
    return el.scrollHeight > el.clientHeight;
  }

  function onResize() {
    startTimeout();
  }

  function registerOnChangeListener(fn) {
    onChangeListeners.push(fn);
  }

  function notifyOnChangeListeners() {
    onChangeListeners.forEach(fn => fn());
  }

  function startTimeout() {
    if (!Number.isNaN(timeoutId)) {
      return;
    }
    timeoutId = setTimeout(() => {
      stopTimeout();
      updateValue();
    }, 200);
  }

  function stopTimeout() {
    clearTimeout(timeoutId);
    timeoutId = Number.NaN;
  }

  function updateValue() {
    const nextResult = getResult();
    if (nextResult === result) {
      return;
    }
    result = nextResult;
    notifyOnChangeListeners();
  }
}

export function injectCssStyles(cssCode) {
  const el = document.createElement('style');
  el.setAttribute('type', 'text/css');
  el.appendChild(document.createTextNode(cssCode));
  document.querySelector('head').appendChild(el);
}
