module.exports = {
  root: true,
  extends: 'airbnb-base',
  plugins: [],
  env: {
    browser: true,
    node: true
  },
  settings: {
    'import/resolver': {
      node: {}, // https://github.com/benmosher/eslint-plugin-import/issues/1396
      webpack: {
        config: './dev-tools/webpack-config/index.js'
      }
    }
  },
  rules: {
    'comma-dangle': ['error', 'never'],
    'no-param-reassign': 'off',
    'no-use-before-define': ['error', 'nofunc'],
    'no-plusplus': 'off',
    'func-names': 'off',
    'default-case': 'off',
    'no-shadow': 'off',
    'vars-on-top': 'off',
    'max-len': ['error', 120],
    'import/imports-first': 'off',
    'import/prefer-default-export': 'off',
    'curly': ['error', 'all'],
    'brace-style': ['error', '1tbs', { allowSingleLine: false }],
    'no-mixed-operators': ['error', {
      allowSamePrecedence: true
    }],
    'arrow-parens': ['error', 'as-needed', { requireForBlockBody: true }],
    'object-curly-newline': 'off',

    // these settings was copy/pasted from node_modules/eslint-config-airbnb-base/rules/style.js
    // The field MemberExpression was modified
    indent: ['error', 2, {
      SwitchCase: 1,
      VariableDeclarator: 1,
      outerIIFEBody: 1,
      MemberExpression: 1,
      FunctionDeclaration: {
        parameters: 1,
        body: 1
      },
      FunctionExpression: {
        parameters: 1,
        body: 1
      }
    }]
  }
};
