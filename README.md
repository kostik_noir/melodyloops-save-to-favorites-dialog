# Setup

+ run `yarn install`
+ create file `.env` in the root of the project. See `.env.example` as an example.

# Dev

+ `yarn dist` - build release version of the application
+ `yarn dev` - run dev server and build dev version of the application

# Fake API server

By default, this server is available at http://localhost:3001.

If you need to change its address then

1. copy `.env.example` to `.env`
2. change the values
3. change the value of `API_SERVER` in `src/demo/index.html`
