const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { isProductionMode, projectRootDir } = require('../../project-config');

const plugins = [
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify(process.env.NODE_ENV)
    }
  })
];

if (isProductionMode) {
  plugins.push(new CleanWebpackPlugin());
}

module.exports = () => ({
  context: projectRootDir,
  target: 'web',
  mode: isProductionMode ? 'production' : 'development',
  entry: null,
  output: null,
  module: {
    rules: [],
    noParse: []
  },
  resolve: {
    alias: {}
  },
  performance: {},
  optimization: {
    minimize: isProductionMode,
    minimizer: []
  },
  plugins
});
