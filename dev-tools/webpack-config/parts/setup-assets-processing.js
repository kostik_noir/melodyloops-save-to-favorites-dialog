const { isProductionMode } = require('../../project-config');

const assetNamePattern = 'assets/[name]_[hash].[ext]';

const imagesRule = {
  test: /\.(gif|png|jpe?g|ico|svg)$/,
  use: [
    {
      loader: 'url-loader',
      options: {
        limit: 5000,
        name: assetNamePattern
      }
    }
  ]
};

if (isProductionMode) {
  imagesRule.use.unshift({
    loader: 'image-webpack-loader'
  });
}

module.exports = (webpackCfg) => {
  webpackCfg.module.rules.push(imagesRule);
};
