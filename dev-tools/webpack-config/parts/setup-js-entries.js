const path = require('path');
const { sourceRootDir } = require('../../project-config');

module.exports = (webpackConfig) => {
  webpackConfig.entry = {
    demo: path.resolve(sourceRootDir, 'demo/index.js'),
    index: path.resolve(sourceRootDir, 'save-to-favorites-dialog/index.js')
  };
};
