const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { isProductionMode, sourceRootDir } = require('../../project-config');

const miniCssExtractPluginLoaderCfg = {
  loader: MiniCssExtractPlugin.loader
};

const styleLoaderCfg = {
  loader: 'style-loader'
};

const cssLoaderCfg = {
  loader: 'css-loader',
  options: {
    sourceMap: !isProductionMode,
    importLoaders: 3,
    modules: {
      mode: 'global',
      localIdentName: isProductionMode ? '[hash:base64]' : '[path][name]__[local]',
      context: sourceRootDir
    }
  }
};

const resolveUrlLoaderCfg = {
  loader: 'resolve-url-loader',
  options: {
    sourceMap: !isProductionMode
  }
};

const postCssLoaderCfg = {
  loader: 'postcss-loader'
};

const sassLoaderCfg = {
  loader: 'sass-loader',
  options: {
    sourceMap: true // it's required for resolve-url-loader
  }
};

module.exports = (webpackConfig) => {
  webpackConfig.module.rules.push({
    test: /\.css$/,
    use: [
      isProductionMode ? miniCssExtractPluginLoaderCfg : styleLoaderCfg,
      cssLoaderCfg,
      postCssLoaderCfg,
      resolveUrlLoaderCfg
    ]
  });

  webpackConfig.module.rules.push({
    test: /\.(scss|sass)$/,
    use: [
      isProductionMode ? miniCssExtractPluginLoaderCfg : styleLoaderCfg,
      cssLoaderCfg,
      postCssLoaderCfg,
      resolveUrlLoaderCfg,
      sassLoaderCfg
    ]
  });

  if (isProductionMode) {
    webpackConfig.plugins.push(new MiniCssExtractPlugin({
      filename: '[name].css'
    }));
  }
};
