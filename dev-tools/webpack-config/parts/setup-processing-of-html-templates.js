const { isProductionMode } = require('../../project-config');

module.exports = (webpackCfg) => {
  webpackCfg.module.rules.push({
    test: /\.?tpl.html/,
    use: {
      loader: 'html-loader',
      options: {
        minimize: isProductionMode
      }
    }
  });
};
