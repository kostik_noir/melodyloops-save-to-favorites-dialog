const { distRootDir, publicPath } = require('../../project-config');

module.exports = (webpackCfg) => {
  webpackCfg.output = {
    path: distRootDir,
    publicPath,
    filename: '[name].js',
    chunkFilename: '[name]-[chunkhash].chunk.js'
  };
};
