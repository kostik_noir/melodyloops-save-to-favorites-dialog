const os = require('os');
const { sourceRootDir, isProductionMode } = require('../../project-config');

const host = os.platform() === 'linux' ? '0.0.0.0' : '127.0.0.1';
const port = 3000;

module.exports = (webpackCfg) => {
  if (isProductionMode) {
    return;
  }

  webpackCfg.devServer = {
    host,
    port,
    historyApiFallback: true,
    stats: 'minimal',
    contentBase: sourceRootDir,
    disableHostCheck: true
  };
};
