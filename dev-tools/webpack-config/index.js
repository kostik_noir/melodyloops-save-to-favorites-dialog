const createSeed = require('./parts/create-seed');
const setupOutput = require('./parts/setup-output');
const setupJsEntries = require('./parts/setup-js-entries');
const setupHtmlEntries = require('./parts/setup-html-entries');
const setupDebugging = require('./parts/setup-debugging');
const setupDevServer = require('./parts/setup-dev-server');
const setupJsCodeProcessing = require('./parts/setup-js-code-processing');
const setupStylesheetsProcessing = require('./parts/setup-stylesheets-processing');
const setupAssetsProcessing = require('./parts/setup-assets-processing');
const setupHtmlTemplatesProcessing = require('./parts/setup-processing-of-html-templates');

const cfg = createSeed();

setupJsEntries(cfg);
setupHtmlEntries(cfg);

setupOutput(cfg);

setupDebugging(cfg);
setupDevServer(cfg);

setupJsCodeProcessing(cfg);
setupStylesheetsProcessing(cfg);
setupAssetsProcessing(cfg);
setupHtmlTemplatesProcessing(cfg);

module.exports = cfg;
