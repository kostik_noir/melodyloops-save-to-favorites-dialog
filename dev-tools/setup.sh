yarn init -y

# ==================
# webpack
# ==================
yarn add -D webpack webpack-cli webpack-dev-server clean-webpack-plugin

# ==================
# JavaScript
# ==================
yarn add -D babel-loader @babel/core @babel/preset-env terser-webpack-plugin core-js@3.3.3
yarn add -D eslint eslint-config-airbnb-base eslint-import-resolver-webpack eslint-plugin-import

# ==================
# stylesheets
# ==================
yarn add -D style-loader css-loader sass-loader node-sass mini-css-extract-plugin postcss-loader resolve-url-loader
yarn add -D autoprefixer postcss-preset-env cssnano

# ==================
# files
# ==================
yarn add -D file-loader url-loader image-webpack-loader

# ==================
# HTML entries
# ==================
yarn add -D html-webpack-plugin
