module.exports = {
  root: true,
  extends: '../.eslintrc.js',
  rules: {
    'no-console': 'off',
    'import/no-extraneous-dependencies': ['error', {
      devDependencies: true
    }],
    'no-await-in-loop': 'off',
    'no-continue': 'off',
    'prefer-destructuring': ['error', {
      array: false
    }],
    'prefer-object-spread': 'off'
  }
};
